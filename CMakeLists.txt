cmake_minimum_required(VERSION 2.6)

set(projname cppformat)
project(${projname})

include_directories(src)

add_library(${projname} STATIC src/format.h src/format.cc)
set_target_properties(${projname} PROPERTIES LINKER_LANGUAGE CXX)